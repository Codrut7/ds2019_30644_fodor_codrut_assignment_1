package ro.codrut.medicalclinic.model;

public class Doctor {

    private Long doctorId;
    private String name;
    private String hosptial;
    private User user;


    public Doctor(Long doctorId, String name, String hosptial, User user) {
        this.doctorId = doctorId;
        this.name = name;
        this.hosptial = hosptial;
        this.user = user;
    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHosptial() {
        return hosptial;
    }

    public void setHosptial(String hosptial) {
        this.hosptial = hosptial;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
