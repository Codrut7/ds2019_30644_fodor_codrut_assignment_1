package ro.codrut.medicalclinic.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import ro.codrut.medicalclinic.model.Caregiver;

public interface CaregiverAPIService {

    @GET("caregiver/details")
    Call<Caregiver> getCareviver();
}
