package ro.codrut.medicalclinic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.codrut.medicalclinic.model.Caregiver;
import ro.codrut.medicalclinic.model.Medication;
import ro.codrut.medicalclinic.model.Patient;
import ro.codrut.medicalclinic.rest.ApiServiceGenerator;
import ro.codrut.medicalclinic.rest.CaregiverAPIService;
import ro.codrut.medicalclinic.rest.PatientAPIService;


public class CaregiverActivity extends AppCompatActivity {

    @BindView(R.id.caregiverText1)
    TextView textView1;
    @BindView(R.id.caregiverText2)
    TextView textView3;
    @BindView(R.id.caregiverText3)
    TextView textView4;
    @BindView(R.id.caregiverText4)
    TextView textView5;
    @BindView(R.id.caregiverText5)
    TextView textView6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("fail1");
        setContentView(R.layout.activity_caregiver);
        ButterKnife.bind(this);
        setInfo();
    }

    private void setInfo() {
        CaregiverAPIService caregiverAPIService =
                ApiServiceGenerator.createService(CaregiverAPIService.class, ApiServiceGenerator.getToken());
        Call<Caregiver> call = caregiverAPIService.getCareviver();
        call.enqueue(new Callback<Caregiver>() {

            @Override
            public void onResponse(Call<Caregiver> call, Response<Caregiver> response) {
                textView1.setText(" DETAILS OF THE CAREGIVER ");
                textView3.setText("Name : " + response.body().getName());
                textView5.setText("Address : " + response.body().getAddress());
                StringBuilder patients = new StringBuilder();
                for(Patient m: response.body().getPatients()){
                    patients.append(m.getName()+" ");
                }
                textView6.setText("Patients : " + patients.toString());
                textView4.setText("Birth date : ");
            }

            @Override
            public void onFailure(Call<Caregiver> call, Throwable t) {

            }
        });
    }
}
