package ro.codrut.medicalclinic;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.codrut.medicalclinic.model.Caregiver;
import ro.codrut.medicalclinic.model.Medication;
import ro.codrut.medicalclinic.model.Patient;
import ro.codrut.medicalclinic.rest.ApiServiceGenerator;
import ro.codrut.medicalclinic.rest.PatientAPIService;


public class PatientActivity extends AppCompatActivity {

    @BindView(R.id.textView)
    TextView textView1;
    @BindView(R.id.button1)
    Button button;
    @BindView(R.id.button2)
    Button viewSelf;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.textView6)
    TextView textView6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("fail1");
        setContentView(R.layout.activity_patient);
        ButterKnife.bind(this);
        System.out.println("fail2");
        setInfo();
    }

    private void setInfo() {
        PatientAPIService patientAPIService =
                ApiServiceGenerator.createService(PatientAPIService.class, ApiServiceGenerator.getToken());
        Call<Patient> call = patientAPIService.viewDetailsPatient();
        call.enqueue(new Callback<Patient>() {
            @Override
            public void onResponse(Call<Patient> call, Response<Patient> response) {
                Log.d("Message",response.body().getName()+ " " + response.body().getAddress()+ " " + response.body().getGender() + " " + response.body().getMedicalRecord());
                textView1.setText(" DETAILS OF THE PATIENT ");
                textView3.setText("Name : " + response.body().getName());
                textView5.setText("Address : " + response.body().getAddress());
                StringBuilder medications = new StringBuilder();
                for(Medication m: response.body().getMedicationPlan().getMedications()){
                    medications.append(m.getName() + " ");
                }
                textView6.setText("Medications : " + medications.toString());
                textView4.setText("Birth date : ");

            }

            @Override
            public void onFailure(Call<Patient> call, Throwable t) {
                System.out.println("fail");
            }
        });
    }


    @OnClick(R.id.button1)
    public void viewCaregiver() {
        PatientAPIService patientAPIService =
                ApiServiceGenerator.createService(PatientAPIService.class, ApiServiceGenerator.getToken());
        Call<Caregiver> call = patientAPIService.getCaregiverCall();
        call.enqueue(new Callback<Caregiver>() {
            @Override
            public void onResponse(Call<Caregiver> call, Response<Caregiver> response) {
                textView1.setText(" DETAILS OF YOUR CAREGIVER ");
                textView3.setText("Name : " + response.body().getName());
                textView5.setText("Address : " + response.body().getAddress());
                textView6.setText("Gender : "  +response.body().getGender());
            }

            @Override
            public void onFailure(Call<Caregiver> call, Throwable t) {

            }
        });
    }
    @OnClick(R.id.button2)
    public void setViewSelf(){
        setInfo();
    }
}
