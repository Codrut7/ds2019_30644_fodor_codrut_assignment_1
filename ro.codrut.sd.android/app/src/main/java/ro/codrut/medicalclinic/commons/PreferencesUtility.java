package ro.codrut.medicalclinic.commons;

import android.content.Context;

public final class PreferencesUtility {

    private static android.content.SharedPreferences sharedPreferences;
    private static final int INT_DEFAULT_VALUE = 0;
    private static final String STRING_DEFAULT_VALUE = "";

    private PreferencesUtility() {
    }


    public static void init(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getApplicationContext().getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        }
    }


    public static int read(String key) {
        return sharedPreferences.getInt(key, INT_DEFAULT_VALUE);
    }

    public static void write(String key, Integer value) {
        android.content.SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
        preferencesEditor.putInt(key, value).apply();
    }

    public static String readString(String key) {
        return sharedPreferences.getString(key, STRING_DEFAULT_VALUE);
    }


    public static void writeString(String key, String value) {
        android.content.SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
        preferencesEditor.putString(key, value).apply();
    }


    public static void clear() {
        android.content.SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
        preferencesEditor.clear();
        preferencesEditor.apply();
    }

}
