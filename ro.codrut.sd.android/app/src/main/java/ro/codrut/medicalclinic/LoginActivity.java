package ro.codrut.medicalclinic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.codrut.medicalclinic.commons.PreferencesUtility;
import ro.codrut.medicalclinic.model.Caregiver;
import ro.codrut.medicalclinic.model.User;
import ro.codrut.medicalclinic.rest.ApiServiceGenerator;
import ro.codrut.medicalclinic.rest.UserAPIService;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.input_username)
    EditText userUsername;
    @BindView(R.id.input_password)
    EditText userPassword;
    private UserAPIService userAPIService;
    private static final String TAG = "LoginActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        userAPIService = ApiServiceGenerator.createService(UserAPIService.class);
    }

    @OnClick(R.id.login_button)
    public void loginOperation() {
        UserAPIService loginService =
                ApiServiceGenerator.createService(UserAPIService.class, userUsername.getText().toString(), userPassword.getText().toString());
        Call<User> call = loginService.loginUser();
        call.enqueue(new Callback<User >() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    switch (response.body().getRoles()){
                        case "DOCTOR":
                            startActivity(new Intent(LoginActivity.this, DoctorActivity.class));
                            finish();
                            break;
                        case "PATIENT":
                            System.out.println("AM AJUNS LA LOG");
                            startActivity(new Intent(LoginActivity.this, PatientActivity.class));
                            finish();
                            break;
                        case "CAREGIVER":
                            startActivity(new Intent(LoginActivity.this, CaregiverActivity.class));
                            finish();
                            break;
                    }
                } else {
                    // error response, no access to resource?
                    System.out.println(response.code());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
            }


    });

    }

    private void goToHomePage() {
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        this.finish();
    }
}
