package ro.codrut.medicalclinic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import ro.codrut.medicalclinic.rest.ApiServiceGenerator;
import ro.codrut.medicalclinic.rest.UserAPIService;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.finish();
    }

}
