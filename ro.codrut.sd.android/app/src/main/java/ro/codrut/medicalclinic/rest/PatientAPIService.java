package ro.codrut.medicalclinic.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import ro.codrut.medicalclinic.model.Caregiver;
import ro.codrut.medicalclinic.model.Patient;

public interface PatientAPIService {

    @GET("/patient/details")
    Call<Patient> viewDetailsPatient();

    @GET("/patient/getCaregiver")
    Call<Caregiver> getCaregiverCall();
}
