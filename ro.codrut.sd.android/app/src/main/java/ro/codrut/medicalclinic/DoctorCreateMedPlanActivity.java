package ro.codrut.medicalclinic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.codrut.medicalclinic.model.Doctor;
import ro.codrut.medicalclinic.rest.ApiServiceGenerator;
import ro.codrut.medicalclinic.rest.DoctorAPIService;

public class DoctorCreateMedPlanActivity extends AppCompatActivity {

    @BindView(R.id.medPlanEditText)
    EditText medPlanEditText;
    @BindView(R.id.medPlanEndDateEdit)
    EditText medPlanEndDateText;
    @BindView(R.id.medPlanStartDateEdit)
    EditText medPlanStartDateText;
    @BindView(R.id.medPlanIntakeEdit)
    EditText medPlanIntakeText;
    @BindView(R.id.medicationsEdit)
    EditText medicationsText;
    @BindView(R.id.createPlan)
    Button createPlan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createmedplan);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.createPlan)
    public void createMedPlan(){
        DoctorAPIService doctorAPIService =
                ApiServiceGenerator.createService(DoctorAPIService.class, ApiServiceGenerator.getToken());
        List<String> meds = new ArrayList<>();
        meds.add(medicationsText.getText().toString());
        Call<Boolean> call = doctorAPIService.createMedicationPlan(medPlanEditText.getText().toString(),meds,medPlanStartDateText.getText().toString(),medPlanEndDateText.getText().toString(),25);
        call.enqueue(new Callback<Boolean>() {

            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                System.out.println("ceeva");
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }
}
