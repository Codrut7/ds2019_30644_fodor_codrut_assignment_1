package ro.codrut.medicalclinic.rest;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import ro.codrut.medicalclinic.model.Doctor;
import ro.codrut.medicalclinic.model.Medication;

public interface DoctorAPIService {

    @GET("/doctor/viewDetails")
    Call<Doctor> viewDetails();

    @GET("/doctor/viewMedications")
    Call<Medication> viewMedications();

    @DELETE("/doctor/deleteMedication")
    Call<Void> deleteMedication();

    @PUT("/doctor/createMedicationPlan")
    Call<Boolean> createMedicationPlan(@Query("name")String patientName, @Query("medications")List<String> medicationNames, @Query("startDate") String startDate, @Query("endDate") String endDate, @Query("intake")int intake);
}
