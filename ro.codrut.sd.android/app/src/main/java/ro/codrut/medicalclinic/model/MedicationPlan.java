package ro.codrut.medicalclinic.model;

import java.util.Date;
import java.util.Set;

public class MedicationPlan {

    private Long medicationPlanId;
    private Set<Medication> medications;
    private Date startDate;
    private Date endDate;
    private int intakeNumber;


    public MedicationPlan(Long medicationPlanId, Set<Medication> medications, Date startDate, Date endDate, int intakeNumber) {
        this.medicationPlanId = medicationPlanId;
        this.medications = medications;
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeNumber = intakeNumber;
    }


    public Long getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getIntakeNumber() {
        return intakeNumber;
    }

    public void setIntakeNumber(int intakeNumber) {
        this.intakeNumber = intakeNumber;
    }
}
