package ro.codrut.medicalclinic.rest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import ro.codrut.medicalclinic.model.LoginPerson;
import ro.codrut.medicalclinic.model.User;

public interface UserAPIService {

    @PUT("/doctor/createPatient")
    Call<Boolean> registerUser(@Query("username") String username, @Query("name") String name, @Query("password") String password, @Query("birthday") String birthday, @Query("gender") String gender, @Query("address") String address);

    @GET("/profile/details")
    Call<User> loginUser();

}
