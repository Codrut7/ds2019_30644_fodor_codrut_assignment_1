package ro.codrut.medicalclinic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.codrut.medicalclinic.model.Doctor;
import ro.codrut.medicalclinic.model.User;
import ro.codrut.medicalclinic.rest.ApiServiceGenerator;
import ro.codrut.medicalclinic.rest.DoctorAPIService;
import ro.codrut.medicalclinic.rest.UserAPIService;

public class DoctorActivity extends AppCompatActivity {

    @BindView(R.id.doctorTitle)
    TextView textTitle;
    @BindView(R.id.doctorName)
    TextView textName;
    @BindView(R.id.doctorHospital)
    TextView textHospital;
    @BindView(R.id.create)
    Button createButton;
    @BindView(R.id.deleteDoctor)
    Button deleteDoctor;
    @BindView(R.id.get)
    Button getMedicationPlan;
    @BindView(R.id.update)
    Button updateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("fail1");
        setContentView(R.layout.activity_doctor);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.get)
    public void viewDetails() {
        DoctorAPIService doctorAPIService =
                ApiServiceGenerator.createService(DoctorAPIService.class, ApiServiceGenerator.getToken());
        Call<Doctor> call = doctorAPIService.viewDetails();
        call.enqueue(new Callback<Doctor>() {
            @Override
            public void onResponse(Call<Doctor> call, Response<Doctor> response) {
                System.out.println(response);
                textTitle.setText("DOCTOR PAGE ");
                textName.setText("Name of the doctor is : " + response.body().getName());
                textHospital.setText("Hosopital of the doctor is + " + response.body().getHosptial());

            }

            @Override
            public void onFailure(Call<Doctor> call, Throwable t) {
                System.out.println("dragos e prost");
            }
        });
    }

    @OnClick(R.id.create)
    public void createMedPlan(){
        startActivity(new Intent(DoctorActivity.this, DoctorCreateMedPlanActivity.class));
        this.finish();
    }
}
