package project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Class used to represent the patient entity in the project.
 *
 * @author Codrut
 */
@Entity
public class Patient {

    /** ID of the patient. */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long patientId;

    /** ID of the caregiver assigned to the patient. */
    @ManyToOne
    @JoinColumn(name="caregiverId", nullable=false)
    @JsonIgnore
    private Caregiver caregiver;

    @OneToOne
    @JoinColumn(name="id")
    private User user;

    /** Name of the patient. */
    private String name;

    /** Birth date of the patient. */
    private Date birthDate;

    /** Gender of the patient. */
    private String gender;

    /** Adress of the patient. */
    private String address;

    /** Medical record of the patient. */
    private String medicalRecord;

    /** Patient that gets the medication plan. */
    @OneToOne
    @JoinColumn(name="medicationPlanId")
    private MedicationPlan medicationPlan;

    public Patient(Caregiver caregiver, User user, String name, Date birthDate, String gender, String address, String medicalRecord) {
        this.caregiver = caregiver;
        this.user = user;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public Patient(){

    }


    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
