package project.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class MedicationPlan {

    /** ID of the medication. */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long medicationPlanId;

    /** List of medications of the plan.*/
    @ManyToMany(mappedBy = "medicationPlans")
    private Set<Medication> medications;

    /** Start date of the medication plan. */
    private Date startDate;

    /** End date of the medication plan. */
    private Date endDate;

    /** Daily intake number. */
    private int intakeNumber;

    public MedicationPlan(Set<Medication> medications, Date startDate, Date endDate, int intakeNumber, Patient patient) {
        this.medications = medications;
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeNumber = intakeNumber;
    }

    public MedicationPlan(){

    }

    public Long getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getIntakeNumber() {
        return intakeNumber;
    }

    public void setIntakeNumber(int intakeNumber) {
        this.intakeNumber = intakeNumber;
    }

}
