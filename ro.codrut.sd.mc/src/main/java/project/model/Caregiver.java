package project.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Class used to represent the caregiver entity in the project.
 *
 * @author Codrut
 */
@Entity
public class Caregiver {

    /** ID of the caregiver. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long caregiverId;

    /** Name of the caregiver. */
    private String name;

    /** Birth date of the caregiver. */
    private Date birthDate;

    /** Gender of the caregiver. */
    private String gender;

    /** Adress of the caregiver. */
    private String address;

    /** List of patients of the caregiver. */
    @OneToMany(mappedBy="caregiver")
    private Set<Patient> patients;

    @OneToOne
    @JoinColumn(name="id")
    private User user;


    public Caregiver(String name, Date birthDate, String gender, String address, Set<Patient> patients, User user) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
        this.user = user;
    }

    public Caregiver(){}


    public Long getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Long caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
