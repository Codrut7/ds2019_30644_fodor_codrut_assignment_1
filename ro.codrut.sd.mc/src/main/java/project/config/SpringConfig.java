package project.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import project.service.*;

@Configuration
public class SpringConfig {

    @Bean
    public PatientServiceLayer getPatientServiceLayer(){
        return new PatientServiceLayer();
    }

    @Bean
    public UserServiceLayer getUserServiceLayer(){
        return new UserServiceLayer();
    }

    @Bean
    public CaregiverServiceLayer getCaregiverServiceLayer(){
        return new CaregiverServiceLayer();
    }

    @Bean
    public DoctorServiceLayer getDoctorServiceLayer(){ return new DoctorServiceLayer(); }

    @Bean
    public MedicationServiceLayer getMedicationServiceLayer() {return new MedicationServiceLayer(); }

    @Bean
    public MedicationPlanServiceLayer getMedicationPlanServiceLayer() {return new MedicationPlanServiceLayer(); }
}
