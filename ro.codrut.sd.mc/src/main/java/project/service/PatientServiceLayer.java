package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import project.dao.PatientRepository;
import project.model.Patient;
import project.model.User;

import java.util.ArrayList;
import java.util.List;


/**
 * Service layers used to validate the information given for the project queries.
 *
 * @author Codrut
 */
public class PatientServiceLayer {

    @Autowired
    private PatientRepository patientRepository;

    /**
     * Return all the available patients.
     *
     * @return
     */
    public List<Patient> getAlPatientList(){
        List<Patient> patients = new ArrayList<>();
        patientRepository.findAll().forEach(patients::add);
        return patients;
    }

    /**
     * Return a patient by the given id.
     *
     * @param user of the patient
     */
    public Patient findPatientByUser(User user){
        return patientRepository.findByUser(user);
    }

    /**
     * Create a new patient entity in the project.
     *
     * @param patient
     */
    public void createPatient(Patient patient){
        patientRepository.save(patient);
    }

    /**
     * Delete a patient entity from the project by the given @id.
     *
     * @param id
     */
    public void deletePatient(Long id){
        patientRepository.deleteById(id);
    }
}
