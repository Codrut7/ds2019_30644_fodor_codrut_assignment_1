package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import project.dao.UserRepository;
import project.model.User;
import project.utils.Validators;

public class UserServiceLayer {

    @Autowired
    private UserRepository userRepository;

    /**
     * Perform the register operation the patient after validating username/password.
     *
     * @param user
     * @param rawPassword
     * @return
     */
    public boolean register(User user, String rawPassword){
        if(Validators.validateUsername(user.getUsername()) && Validators.validatePassword(rawPassword)){
            userRepository.save(user);
            return true;
        }
       return false;
    }

    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public boolean deleteUser(User user){
        userRepository.delete(user);
        return true;
    }
}
