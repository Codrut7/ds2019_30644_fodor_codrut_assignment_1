package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import project.model.*;
import project.service.*;

import java.util.*;

@RestController
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    private PatientServiceLayer patientServiceLayer;
    @Autowired
    private UserServiceLayer userServiceLayer;
    @Autowired
    private DoctorServiceLayer doctorServiceLayer;
    @Autowired
    private CaregiverServiceLayer caregiverServiceLayer;
    @Autowired
    private MedicationServiceLayer medicationServiceLayer;
    @Autowired
    private MedicationPlanServiceLayer medicationPlanServiceLayer;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/getPatients")
    public List<Patient> getAllPatients() {
        return patientServiceLayer.getAlPatientList();
    }

    @GetMapping("/getCaregivers")
    public List<Caregiver> getAllCaregivers() {
        return caregiverServiceLayer.getAllCaregiversList();
    }

    @GetMapping("/viewDetails")
    public Doctor viewDetails() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Doctor doctor = doctorServiceLayer.findDoctorByUser(user);

        return doctor;
    }

    @GetMapping("/viewMedications")
    public List<Medication> getAllMedications(){
        return medicationServiceLayer.getAllMedications();
    }

    @DeleteMapping("/deleteCaregiver")
    public void deleteCaregiver(@RequestParam("name") String name) {
        User user = userServiceLayer.findByUsername(name);
        caregiverServiceLayer.deleteCaregiver(user.getId());
        userServiceLayer.deleteUser(user);
    }

    @DeleteMapping("/deletePatient")
    public void deletePatient(@RequestParam("name") String name) {
        User user = userServiceLayer.findByUsername(name);
        patientServiceLayer.deletePatient(user.getId());
        userServiceLayer.deleteUser(user);
    }

    @DeleteMapping("/deleteMedication")
    public void deleteMedication(@RequestParam("name") String name){
        Medication medication = medicationServiceLayer.findByName(name);
        medicationServiceLayer.deleteMedication(medication);
    }

    @DeleteMapping("/deleteAccount")
    public void deleteSelf() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Doctor doctor = doctorServiceLayer.findDoctorByUser(user);
        doctorServiceLayer.deleteDoctor(doctor.getDoctorId());
        userServiceLayer.deleteUser(user);
    }

    @PutMapping("/createCaregiver")
    public boolean createCaregiver(@RequestParam("username") String username, @RequestParam("name") String name, @RequestParam("password") String password, @RequestParam("birthday") String birthday, @RequestParam("gender") String gender, @RequestParam("address") String address) {
        User user = new User(username, passwordEncoder.encode(password), "CAREGIVER", "CAREGIVER");
        if (userServiceLayer.register(user, password)) {
            Caregiver caregiver = new Caregiver(name, new Date(), gender, address, null, user);
            caregiverServiceLayer.createCaregiver(caregiver);
            return true;
        }
        return false;
    }

    @PutMapping("/createPatient")
    public boolean register(@RequestParam("username") String username, @RequestParam("name") String name, @RequestParam("password") String password, @RequestParam("birthday") String birthday, @RequestParam("gender") String gender, @RequestParam("address") String address) {
        User user = new User(username,passwordEncoder.encode(password),"PATIENT","PATIENT");
        List<Caregiver> caregiverList = caregiverServiceLayer.getAllCaregiversList();
        System.out.println(caregiverList.get(0).getName());
        if(userServiceLayer.register(user, password)){
            Caregiver caregiver = caregiverList.get(0);
            Patient patient = new Patient(caregiver,user,name,new Date(System.currentTimeMillis()),gender,address,"");
            patientServiceLayer.createPatient(patient);
            return true;
        }

        return false;
    }

    @PutMapping("/createMedication")
    public boolean createMedication(@RequestParam("name")String name,@RequestParam("sideEffects")List<String> sideEffects,@RequestParam("dosage") double dosage){
        Medication medication = new Medication(name,sideEffects,dosage);
        medicationServiceLayer.createMedication(medication);
        return true;
    }

    @PostMapping("/updateCaregiver")
    public boolean updateCaregiver(@RequestParam("username") String username, @RequestParam("name") String name, @RequestParam("birthday") String birthday, @RequestParam("gender") String gender, @RequestParam("address") String address){
        User user = userServiceLayer.findByUsername(username);
        Set<Patient> patients = caregiverServiceLayer.findCaregiveByUser(user).getPatients();
        Caregiver caregiver = new Caregiver(name,null,gender,address,patients,user);

        caregiverServiceLayer.createCaregiver(caregiver);

        return true;
    }

    @PostMapping("/updatePatients")
    public boolean updatePatients(@RequestParam("username") String username, @RequestParam("name") String name, @RequestParam("birthday") String birthday, @RequestParam("gender") String gender, @RequestParam("address") String address, @RequestParam("medicalRecord") String medicalRecord){
        User user = userServiceLayer.findByUsername(username);
        Patient patient = patientServiceLayer.findPatientByUser(user);

        Patient updatedPatient = new Patient(patient.getCaregiver(),user,name,null,gender,address, medicalRecord);

        patientServiceLayer.createPatient(updatedPatient);

        return true;
    }

    @PostMapping("/updateMedication")
    public boolean updateMedication(@RequestParam("name")String name,@RequestParam("sideEffects")List<String> sideEffects,@RequestParam("dosage") double dosage){
        Medication medication = medicationServiceLayer.findByName(name);
        medicationServiceLayer.deleteMedication(medication);
        Medication medication1 = new Medication(name,sideEffects,dosage);
        medicationServiceLayer.createMedication(medication1);

        return true;
    }

    @PutMapping("/createMedicationPlan")
    public boolean createMedicationPlan(@RequestParam("name")String patientName, @RequestParam(name="medications",required = true)List<String> medicationNames, @RequestParam("startDate")String startDate, @RequestParam("endDate") String endDate, @RequestParam("intake")int intake){
        User user = userServiceLayer.findByUsername(patientName);
        Patient patient = patientServiceLayer.findPatientByUser(user);
        Set<Medication> medications = new HashSet<>();
        System.out.println("ceetrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrva");
        for(String medication : medicationNames){
            System.out.println(medication);
            medications.add(medicationServiceLayer.findByName(medication));
        }
        MedicationPlan medicationPlan = new MedicationPlan(medications,null,null,intake,patient);
        medicationPlanServiceLayer.createMedicationPlan(medicationPlan);
        for(String medication : medicationNames){
            Medication m = medicationServiceLayer.findByName(medication);
            m.getMedicationPlans().add(medicationPlan);
            medicationServiceLayer.createMedication(m); // daca s-a facut un plan nou care foloseste medicamentul x, trebuie ca medicamentul x sa fie updatat in bd ca e folosit in medication planul respectiv
        }
        patient.setMedicationPlan(medicationPlan);
        patientServiceLayer.createPatient(patient);
        return true;
    }

    @PutMapping("/createDoctor")
    public boolean createDoctor(){
        User user = new User( "admin",passwordEncoder.encode("admin"),"DOCTOR","DOCTOR");
        Doctor doctor = new Doctor("codrut","none",user);
        System.out.println(userServiceLayer.register(user,"admin"));
        doctorServiceLayer.createDoctor(doctor);
        return true;
    }

}
