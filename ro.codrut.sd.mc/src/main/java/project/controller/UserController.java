package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.model.User;
import project.model.UserPrincipal;
import project.service.UserServiceLayer;

@RestController
@RequestMapping("/profile")
public class UserController {

    @Autowired
    private UserServiceLayer userServiceLayer;

    @GetMapping("/details")
    public User viewDetails(){
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        System.out.println(user.getUsername());
        return user;
    }
}
