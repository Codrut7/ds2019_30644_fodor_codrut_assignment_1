package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import project.model.*;
import project.service.CaregiverServiceLayer;
import project.service.PatientServiceLayer;
import project.service.UserServiceLayer;

import java.util.Date;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/caregiver")
public class CaregiverController {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PatientServiceLayer patientServiceLayer;
    @Autowired
    private UserServiceLayer userServiceLayer;
    @Autowired
    private CaregiverServiceLayer caregiverServiceLayer;


    @GetMapping("/details")
    public Caregiver viewDetails() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Caregiver caregiver = caregiverServiceLayer.findCaregiveByUser(user);
        return caregiver;
    }

    // e degeaba
    @GetMapping("/getPatients")
    public Set<Patient> viewPatients() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Caregiver caregiver = caregiverServiceLayer.findCaregiveByUser(user);

        return caregiver.getPatients();
    }

}
