package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.model.*;
import project.service.CaregiverServiceLayer;
import project.service.PatientServiceLayer;
import project.service.UserServiceLayer;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PatientServiceLayer patientServiceLayer;
    @Autowired
    private UserServiceLayer userServiceLayer;
    @Autowired
    private CaregiverServiceLayer caregiverServiceLayer;


    @GetMapping("/details")
    public Patient viewDetails(){
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Patient patient = patientServiceLayer.findPatientByUser(user);
        return patient;
    }

    @GetMapping("/getMedicationPlan")
    public MedicationPlan viewMedicationPlan(){
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Patient patient = patientServiceLayer.findPatientByUser(user);
        return  patient.getMedicationPlan();
    }

    @GetMapping("/getCaregiver")
    public Caregiver getCaregiver(){
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userServiceLayer.findByUsername(userPrincipal.getUsername());
        Patient loggedPatient = patientServiceLayer.findPatientByUser(user);
        List<Caregiver> caregivers = caregiverServiceLayer.getAllCaregiversList();
        for(Caregiver caregiver : caregivers){
            for(Patient patient : caregiver.getPatients()){
                if(patient.getPatientId().equals(loggedPatient.getPatientId())){
                    return caregiver;
                }
            }
        }

        return null;
    }

}
