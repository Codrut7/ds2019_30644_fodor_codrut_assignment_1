CREATE DATABASE  IF NOT EXISTS `sd` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `sd`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sd
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caregiver`
--

DROP TABLE IF EXISTS `caregiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caregiver` (
  `caregiver_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`caregiver_id`),
  KEY `FKckqvmhqppl2ixchdtm9mc5vkh` (`id`),
  CONSTRAINT `FKckqvmhqppl2ixchdtm9mc5vkh` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caregiver`
--

LOCK TABLES `caregiver` WRITE;
/*!40000 ALTER TABLE `caregiver` DISABLE KEYS */;
INSERT INTO `caregiver` VALUES (4,'fsdgfs','2019-10-13 16:10:36','male','mihai',3);
/*!40000 ALTER TABLE `caregiver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `doctor_id` bigint(20) NOT NULL,
  `hosptial` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`doctor_id`),
  KEY `FKisrj7dti092bxya7p8jt7acs7` (`id`),
  CONSTRAINT `FKisrj7dti092bxya7p8jt7acs7` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (2,'none','codrut',1);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (23),(23),(23),(23),(23),(23);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication`
--

DROP TABLE IF EXISTS `medication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication` (
  `medication_id` bigint(20) NOT NULL,
  `dosage` double NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`medication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication`
--

LOCK TABLES `medication` WRITE;
/*!40000 ALTER TABLE `medication` DISABLE KEYS */;
INSERT INTO `medication` VALUES (17,20,'nurofen'),(18,20,'nurofen2');
/*!40000 ALTER TABLE `medication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_plan`
--

DROP TABLE IF EXISTS `medication_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_plan` (
  `medication_plan_id` bigint(20) NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `intake_number` int(11) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  PRIMARY KEY (`medication_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_plan`
--

LOCK TABLES `medication_plan` WRITE;
/*!40000 ALTER TABLE `medication_plan` DISABLE KEYS */;
INSERT INTO `medication_plan` VALUES (12,NULL,20,NULL),(13,NULL,20,NULL),(14,NULL,20,NULL),(19,NULL,20,NULL),(20,NULL,25,NULL),(21,NULL,25,NULL),(22,NULL,25,NULL);
/*!40000 ALTER TABLE `medication_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_side_effects`
--

DROP TABLE IF EXISTS `medication_side_effects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_side_effects` (
  `medication_medication_id` bigint(20) NOT NULL,
  `side_effects` varchar(255) DEFAULT NULL,
  KEY `FK3egrq03ovca8p7uq9dopjkmyv` (`medication_medication_id`),
  CONSTRAINT `FK3egrq03ovca8p7uq9dopjkmyv` FOREIGN KEY (`medication_medication_id`) REFERENCES `medication` (`medication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_side_effects`
--

LOCK TABLES `medication_side_effects` WRITE;
/*!40000 ALTER TABLE `medication_side_effects` DISABLE KEYS */;
INSERT INTO `medication_side_effects` VALUES (17,'\"durere de stomac\"'),(18,'\"durere de stomac\"');
/*!40000 ALTER TABLE `medication_side_effects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medications_plans`
--

DROP TABLE IF EXISTS `medications_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medications_plans` (
  `medication_plan_id` bigint(20) NOT NULL,
  `medication_id` bigint(20) NOT NULL,
  PRIMARY KEY (`medication_plan_id`,`medication_id`),
  KEY `FK4pu12w2440s06mcur3guln29u` (`medication_id`),
  CONSTRAINT `FK4pu12w2440s06mcur3guln29u` FOREIGN KEY (`medication_id`) REFERENCES `medication_plan` (`medication_plan_id`),
  CONSTRAINT `FKfwp0vwx1nqyegkhyhcr42afwx` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication` (`medication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medications_plans`
--

LOCK TABLES `medications_plans` WRITE;
/*!40000 ALTER TABLE `medications_plans` DISABLE KEYS */;
INSERT INTO `medications_plans` VALUES (17,19),(18,19),(17,21),(17,22);
/*!40000 ALTER TABLE `medications_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `patient_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `medical_record` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `caregiver_id` bigint(20) NOT NULL,
  `medication_plan_id` bigint(20) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  KEY `FKrtror6nlor0yjjgcq1csgcp3d` (`caregiver_id`),
  KEY `FK5hur4f1tiy2msg4shsm30dtwl` (`medication_plan_id`),
  KEY `FKbhxnsr0osyqj98qqcexec5edv` (`id`),
  CONSTRAINT `FK5hur4f1tiy2msg4shsm30dtwl` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication_plan` (`medication_plan_id`),
  CONSTRAINT `FKbhxnsr0osyqj98qqcexec5edv` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKrtror6nlor0yjjgcq1csgcp3d` FOREIGN KEY (`caregiver_id`) REFERENCES `caregiver` (`caregiver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (6,'ion','2019-10-13 16:10:50','male','','mihai',4,22,5);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `active` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `permissions` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'$2a$10$hN602I5iVkM6JglkE2PqZ.CRxhCOhnDq9U5g2qriSgIMHfPxsuPN.','DOCTOR','DOCTOR','admin'),(3,1,'$2a$10$QqUjz3jP0x9IM7WBxaXmRuHCkfNIKVahn2V1mlWuNNPjvKU784SE.','CAREGIVER','CAREGIVER','caregiver'),(5,1,'$2a$10$jlS57YbRpvoUTVWsEft8c.Vm7/wSKUZTkM5W.VDuELNYTLW2pP2Oe','PATIENT','PATIENT','mihaita');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-30 22:36:05
